#include "ne.h"
#include "router.h"


typedef enum { false, true } bool;
void myPrintRoutes(struct route_entry *routing_table, int myID, int number_of_routes);
void print_my_routing_table(int myID);

typedef struct my_neighbor_rt
{
	unsigned int neighbor_id;
    bool is_death;
} my_neighbor_rt;

my_neighbor_rt arr_my_neighbor_rt[MAX_ROUTERS];
int global_num_of_neighbor;

// #define DEBUG
/* ----- GLOBAL VARIABLES ----- */
/* Variable      : struct route_entry routingTable[MAX_ROUTERS]
 * Variable Type : Array of type (struct route_entry)
 * USAGE         : Define as a Global Variable in routingtable.c.
 *                 The routingTable will be used by all the functions in routingtable.c.
 *                 #include ne.h in routingtable.c for definitions of struct route_entry and MAX_ROUTERS.
 */
struct route_entry routingTable[MAX_ROUTERS];

/* Variable      : int NumRoutes
 * Variable Type : Integer
 * USAGE         : Define as a Global Variable in routingtable.c.
 *                 This variable holds the number of routes present in the routing table.
 *                 It is initialized on receiving INIT_RESPONSE from Network Emulator
 *                 and is updated in the UpdateRoutes() function, whenever the routingTable changes. 
 */
int NumRoutes;


/* Routine Name    : InitRoutingTbl
 * INPUT ARGUMENTS : 1. (struct pkt_INIT_RESPONSE *) - The INIT_RESPONSE from Network Emulator
 *                   2. int - My router's id received from command line argument.
 * RETURN VALUE    : void
 * USAGE           : This routine is called after receiving the INIT_RESPONSE message from the Network Emulator. 
 *                   It initializes the routing table with the bootstrap neighbor information in INIT_RESPONSE.  
 *                   Also sets up a route to itself (self-route) with next_hop as itself and cost as 0.
 */
////////////////////////////////////////////////////////////////
/*
// pseudocode
for each Element  in nbrcost:
	get the nbr in pkt_INIT_RESPONSE
	append it to routingTable
	update cost of found nbr in routingTable based on cost


	still update routing table for yourself
*/

void InitRoutingTbl (struct pkt_INIT_RESPONSE *InitResponse, int myID){
	/* ----- YOUR CODE HERE ----- */
	
	int i;
	int num_of_neightbor = InitResponse -> no_nbr;

	//update myself to my routing table
	routingTable[0].dest_id = myID;
	routingTable[0].next_hop = myID;
	routingTable[0].cost = 0;
	routingTable[0].path_len = 1;
	routingTable[0].path[0] = myID;

	//update my neighbor to my routing table
	for(i = 1 ; i <= num_of_neightbor; i++)
	{
		routingTable[i].dest_id = InitResponse -> nbrcost[i-1].nbr;
		routingTable[i].next_hop = InitResponse -> nbrcost[i-1].nbr;
		routingTable[i].cost = InitResponse -> nbrcost[i-1].cost;
		routingTable[i].path_len = 2;
		routingTable[i].path[0] = myID;
		routingTable[i].path[1] = InitResponse -> nbrcost[i-1].nbr;
	}

	global_num_of_neighbor = num_of_neightbor;
	for(i = 0; i < num_of_neightbor; i++)
	{
		arr_my_neighbor_rt[i].neighbor_id = InitResponse -> nbrcost[i].nbr;
		arr_my_neighbor_rt[i].is_death = false;
	}

	NumRoutes = num_of_neightbor + 1;
	#ifdef DEBUG
		printf("\n");
		myPrintRoutes(routingTable, myID, NumRoutes );
		printf("\n");
	#endif

	return;
}



/*
	Helpuer function:
		This function will return the next hop from my routing table

*/
unsigned int my_next_hop(int myID, int destination_id)
{

	int i;
	for (i = 0; i < NumRoutes; i++) {
		if (routingTable[i].dest_id == destination_id)
		{
			return routingTable[i].next_hop;
		}
	}

	return MAX_ROUTERS;
}

/*
	Helpuer function:
		This function will true if I am in my neighbor path
*/
bool is_in_neighbor_path(struct pkt_RT_UPDATE *RecvdUpdatePacket, int myID, int destination_id)
{
	// look in update sender (Z) routingTable for dest_id Y
	// when found:
	// 	if I am in path of my sender, return true
	// else return false
	
	int i;
	int j;
	int neighbor_number_of_routes = RecvdUpdatePacket-> no_routes;

	// find destination id
	for (i = 0; i < neighbor_number_of_routes; i++) 
	{
		struct route_entry neighbor_router_route = RecvdUpdatePacket -> route[i];
		if (neighbor_router_route.dest_id == destination_id) 
		{
			// check if I am in neightbor path
			for (j = 0; j < neighbor_router_route.path_len; j++) 
			{
				if (myID == (neighbor_router_route.path[j])) return true;
			}

		}
	}
	return false;
}

/*
	Helpuer function:
		This function will find the arr neighbor index, return -1 if it's not my neighbot
*/
long int find_arr_neighbor_index(unsigned int neighbor_id)
{
	int i;
	
	for(i = 0; i < global_num_of_neighbor; i++)
	{
		if(arr_my_neighbor_rt[i].neighbor_id == neighbor_id)
		{
			return i;
		}
	}

	return -1;
}

/* Routine Name    : UpdateRoutes
 * INPUT ARGUMENTS : 1. (struct pkt_RT_UPDATE *) - The Route Update message from one of the neighbors of the router.
 *                   2. int - The direct cost to the neighbor who sent the update. 
 *                   3. int - My router's id received from command line argument.
 * RETURN VALUE    : int - Return 1 : if the routing table has changed on running the function.
 *                         Return 0 : Otherwise.
 * USAGE           : This routine is called after receiving the route update from any neighbor. 
 *                   The routing table is then updated after running the distance vector protocol. 
 *                   It installs any new route received, that is previously unknown. For known routes, 
 *                   it finds the shortest path using current cost and received cost. 
 *                   It also implements the forced update and split horizon rules. My router's id
 *                   that is passed as argument may be useful in applying split horizon rule.
 * Rules           : When router A sends a routing update message to router B, it must for each router C (that
					 exists in A’s routing table) indicate the cost, next hop, and full path that it uses to reach C.
					 On receiving the update from A, B updates its table, for each router C that is being
					 advertised by router A,
					 If router B does not have an entry for C, then B must add an entry for C.
					 If it has an entry, then it uses the below Path Vector and Forced Update rules.
 * 
 */
int UpdateRoutes(struct pkt_RT_UPDATE *RecvdUpdatePacket, int costToNbr, int myID){
	/* ----- YOUR CODE HERE ----- */

	int i;
	unsigned int neighbor_num_of_routes = RecvdUpdatePacket -> no_routes;
	unsigned int neighbor_id = RecvdUpdatePacket -> sender_id;
	
	unsigned int neighbor_routingtable_index;
	unsigned int my_routingtable_index;
	
	unsigned int my_dest_id;
	unsigned int neighbor_dest_id;
	
	unsigned int neighbor_cost;
	unsigned int my_cost;
	
	unsigned int neighbor_path_len;
	unsigned int* neighbor_path;
	
	bool need_to_add_to_my_routing_table = true;
	bool is_updated = false;
	struct route_entry *neighbor_routing_table = RecvdUpdatePacket->route;

	long int arr_neighbor_index = -1;

	// Not necessary but do it anyways
	if(myID != RecvdUpdatePacket->dest_id)
	{
		perror("Ne is sending to the wrong router!\n");
		return -1;
	}

	//, if it's my neighbor, set arr neighbor to alive
	arr_neighbor_index =  find_arr_neighbor_index(neighbor_id);
	if( arr_neighbor_index != -1)
	{
		arr_my_neighbor_rt[arr_neighbor_index].is_death = false;	
	}


	// loop through sender routing table
	for(neighbor_routingtable_index = 0 ; neighbor_routingtable_index < neighbor_num_of_routes; neighbor_routingtable_index++)
	{
	
		neighbor_dest_id = neighbor_routing_table[neighbor_routingtable_index].dest_id;
		neighbor_cost =  ( (neighbor_routing_table[neighbor_routingtable_index].cost + costToNbr ) > INFINITY) ? INFINITY  :  neighbor_routing_table[neighbor_routingtable_index].cost + costToNbr;
		neighbor_path_len = neighbor_routing_table[neighbor_routingtable_index].path_len;
		neighbor_path = neighbor_routing_table[neighbor_routingtable_index].path;

		#ifdef DEBUG
			printf("\n");
			myPrintRoutes(neighbor_routing_table, neighbor_id, neighbor_num_of_routes);
			myPrintRoutes(routingTable, myID, NumRoutes );
			printf("\n");
			printf("The cost from neighbor to destination is %hd\n", neighbor_cost);
		#endif


		// looping through my routing table to find the destId
		for(my_routingtable_index = 0; my_routingtable_index < NumRoutes; my_routingtable_index++)
		{
			
			my_dest_id = routingTable[my_routingtable_index].dest_id;
			my_cost = routingTable[my_routingtable_index].cost;
			
			// if neighbor routing table destIid is the same as my oruting table dest_id
			if(neighbor_dest_id == my_dest_id)
			{
				#ifdef DEBUG
						printf("/*********************************************/\n");
						printf("The cost from neighbor %hd to destination %hd is %hd\n",neighbor_id , neighbor_dest_id, neighbor_cost);
						printf("The cost from myself to destination %hd is %hd\n",neighbor_dest_id, my_cost);
						printf("My next hop %d\n", my_next_hop(myID,my_dest_id));
						printf("Is in neighbor path %d\n",is_in_neighbor_path(RecvdUpdatePacket, myID, my_dest_id) );
						printf("/*********************************************/\n");		
				 #endif

				// if it's my neighbor and it has already dead don't update it
				arr_neighbor_index = find_arr_neighbor_index(neighbor_dest_id);
				if( arr_neighbor_index >= 0 &&  arr_my_neighbor_rt[arr_neighbor_index].is_death == true)
				{
					need_to_add_to_my_routing_table = false;
					break;
				}

				// perform path vector rules
				if(( (neighbor_cost < my_cost) && (is_in_neighbor_path(RecvdUpdatePacket, myID, my_dest_id) == false) ))
				{
				
						routingTable[my_routingtable_index].cost = neighbor_cost;
						routingTable[my_routingtable_index].next_hop = neighbor_id;
						routingTable[my_routingtable_index].path_len = neighbor_path_len + 1;
						routingTable[my_routingtable_index].path[0] = myID;
							
						for (i = 0; i < neighbor_path_len; i++) {
								routingTable[my_routingtable_index].path[i+1] = neighbor_path[i];
						}
					
						#ifdef DEBUG
						printf("Neightbor %d cost is %d and my cost %d so we need to update\n",neighbor_id , neighbor_cost,my_cost);
						#endif
				
						is_updated = true;
						need_to_add_to_my_routing_table = false;
						break;
				}


				// perform Force update
				if (my_next_hop(myID,my_dest_id) == neighbor_id)
				{
					
					if(is_in_neighbor_path(RecvdUpdatePacket, myID, my_dest_id) == false)
					{
						routingTable[my_routingtable_index].cost = neighbor_cost;
						routingTable[my_routingtable_index].path_len = neighbor_path_len + 1;
						routingTable[my_routingtable_index].path[0] = myID;
							
						for (i = 0; i < neighbor_path_len; i++) {
							routingTable[my_routingtable_index].path[i+1] = neighbor_path[i];
						}

						#ifdef DEBUG
						printf("Force update by Neightbor %d cost to %d is %d and my cost %d so we need to update\n",neighbor_id , my_dest_id ,neighbor_cost,my_cost);
						#endif	

					}
					else
					{
						#ifdef DEBUG
						printf("My id is in neighbor path, so set it to INFINITY\n");
						#endif
						routingTable[my_routingtable_index].cost = INFINITY;
					}
					
					if(my_cost != neighbor_cost)
					{
						is_updated = true;
						need_to_add_to_my_routing_table = false;
						break;
					}
				}

				// don't need to add it to my routing table
				need_to_add_to_my_routing_table = false;
				break;
			}
		}
		
		#ifdef DEBUG
			printf("The need_to_add_to_my_routing_table flag is %d\n", need_to_add_to_my_routing_table);
		#endif
		//if the packet (dest_id) is not in me, add it to my routing table
		if(need_to_add_to_my_routing_table)
		{

			routingTable[NumRoutes].dest_id  = neighbor_dest_id;
			routingTable[NumRoutes].next_hop = neighbor_id;
			routingTable[NumRoutes].cost     = neighbor_cost;
			routingTable[NumRoutes].path_len = neighbor_path_len + 1;

			routingTable[NumRoutes].path[0] = myID;
			for (i = 0; i < neighbor_path_len; i++) {
				routingTable[NumRoutes].path[i+1] = neighbor_path[i];
			}

			NumRoutes++;
			is_updated = true;
			
		}

		need_to_add_to_my_routing_table = true;
	}

	#ifdef DEBUG
		printf("The is_updated flag is %d\n",is_updated);
	#endif
	return is_updated;
}

/* Routine Name    : ConvertTabletoPkt
 * INPUT ARGUMENTS : 1. (struct pkt_RT_UPDATE *) - An empty pkt_RT_UPDATE structure
 *                   2. int - My router's id received from command line argument.
 * RETURN VALUE    : void
 * USAGE           : This routine fills the routing table into the empty struct pkt_RT_UPDATE. 
 *                   My router's id  is copied to the sender_id in pkt_RT_UPDATE. 
 *                   Note that the dest_id is not filled in this function. When this update message 
 *                   is sent to all neighbors of the router, the dest_id is filled.
 */
////////////////////////////////////////////////////////////////
void ConvertTabletoPkt(struct pkt_RT_UPDATE *UpdatePacketToSend, int myID){
	/* ----- YOUR CODE HERE ----- */
	int i; 
	
	// Copying everything into UpdatePacketToSend
	UpdatePacketToSend -> sender_id = myID;
	UpdatePacketToSend -> no_routes = NumRoutes;


	for (i = 0; i < NumRoutes; i++) {
		UpdatePacketToSend -> route[i] = routingTable[i];
	}

	
}



/* Routine Name    : PrintRoutes
 * INPUT ARGUMENTS : 1. (FILE *) - Pointer to the log file created in router.c, with a filename that uses MyRouter's id.
 *                   2. int - My router's id received from command line argument.
 * RETURN VALUE    : void
 * USAGE           : This routine prints the routing table to the log file 
 *                   according to the format and rules specified in the Handout.
 */
////////////////////////////////////////////////////////////////
//It is highly recommended that you do not change this function!
void PrintRoutes (FILE* Logfile, int myID){
	/* ----- PRINT ALL ROUTES TO LOG FILE ----- */
	int i;
	int j;
	for(i = 0; i < NumRoutes; i++){
		fprintf(Logfile, "<R%d -> R%d> Path: R%d", myID, routingTable[i].dest_id, myID);

		/* ----- PRINT PATH VECTOR ----- */
		for(j = 1; j < routingTable[i].path_len; j++){
			fprintf(Logfile, " -> R%d", routingTable[i].path[j]);	
		}
		fprintf(Logfile, ", Cost: %d\n", routingTable[i].cost);
	}
	fprintf(Logfile, "\n");
	fflush(Logfile);
}

void myPrintRoutes(struct route_entry *routing_table, int myID, int number_of_routes)
{
	int i;
	int j;
	for(i = 0; i < number_of_routes  ; i++) {
		printf("<R%d -> R%d> Path: R%d", myID, routing_table[i].dest_id, myID);

		/* ----- PRINT PATH VECTOR ----- */
		for(j = 1; j < routing_table[i].path_len; j++){
			printf(" -> R%d", routing_table[i].path[j]);	
		}
		printf(", Cost: %d\n", routing_table[i].cost);
	}
	printf("\n");

}

void print_my_routing_table(int myID)
{
	int i;
	int j;
	for(i = 0; i < NumRoutes  ; i++) {
		printf("<R%d -> R%d> Path: R%d", myID, routingTable[i].dest_id, myID);

		/* ----- PRINT PATH VECTOR ----- */
		for(j = 1; j < routingTable[i].path_len; j++){
			printf(" -> R%d", routingTable[i].path[j]);	
		}
		printf(", Cost: %d\n", routingTable[i].cost);
	}
	printf("\n");
}

/* Routine Name    : UninstallRoutesOnNbrDeath
 * INPUT ARGUMENTS : 1. int - The id of the inactive neighbor 
 *                   (one who didn't send Route Update for FAILURE_DETECTION seconds).
 *                   
 * RETURN VALUE    : void
 * USAGE           : This function is invoked when a nbr is found to be dead. The function checks all routes that
 *                   use this nbr as next hop, and changes the cost to INFINITY.
 */
////////////////////////////////////////////////////////////////
void UninstallRoutesOnNbrDeath(int DeadNbr){
	/* ----- YOUR CODE HERE ----- */
	// loop through each route in routingTable
	//   if current route's next hop is DeadNbr
	//     change current route's cost to INF

	int i;
	long arr_neighbor_index;

	for (i = 0; i < NumRoutes; i++) {
		if (routingTable[i].next_hop == DeadNbr) {
			routingTable[i].cost = INFINITY;
		}
	}
	
	arr_neighbor_index = find_arr_neighbor_index(DeadNbr);
	if(arr_neighbor_index == -1)
	{
		printf("Something is wrong!\n");
	}
	arr_my_neighbor_rt[arr_neighbor_index].is_death = true;


	return;
}
