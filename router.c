// user defined library
#include "router.h"
#include "ne.h"
#include <pthread.h> 
#include <time.h>
#include <errno.h>

void *udp_thread_function(void * arg);
void *timer_thread_function(void * arg);

typedef enum { false, true } bool;
/**
 *  Structure for storing out neighbors
 */
typedef struct my_neighbor
{
    unsigned int neighbor_id;
    unsigned int my_cost_to_neighbor;
    time_t recent_time_updated;
    bool is_death;
} my_neighbor;

/**
 * Global Variables
 * 
 */
static int router_udp_fd;

struct sockaddr_in dest_sockaddr;
unsigned int dest_sockaddr_len;

pthread_mutex_t lock;
int num_of_neighbor;
int router_id;

time_t beginning_time;
time_t last_update_interval;
time_t last_convergence_timeout_interval;

char final_logfile_name[64];

struct my_neighbor arr_my_neighbor[MAX_ROUTERS];

bool isNotConverged = true;
bool print_permission = false;

pthread_t udp_thread;
pthread_t timer_thread;

FILE *fptr = NULL;

///////////////////////////////////////////////////////////////////

/* Routine Name    : open_udp_fd
 * INPUT ARGUMENTS : 1. port
 * RETURN VALUE    : udp file descriptor
 * USAGE           : set up our own udp port to bind to a port
 */
int open_udp_fd(int port)  
{ 
  int listenfd, optval=1; 
  struct sockaddr_in serveraddr; 
   
  /* Create a socket descriptor */ 
  if ((listenfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) 
    return -1; 
  
  /* Eliminates "Address already in use" error from bind. */ 
  if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR,  
		 (const void *)&optval , sizeof(int)) < 0) 
    return -1; 
 
  /* Listenfd will be an endpoint for all requests to port 
     on any IP address for this host */ 
  bzero((char *) &serveraddr, sizeof(serveraddr)); 
  serveraddr.sin_family = AF_INET;  
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);  
  serveraddr.sin_port = htons((unsigned short)port);  
  
  //  bind function assigns a local protocol address to a socket
  if (bind(listenfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0) 
    return -1; 
  
  return listenfd; 
} 


// /* Routine Name    : open_send_udp_fd
//  * INPUT ARGUMENTS : 1. port 2. hostname
//  * RETURN VALUE    : sockaddr_in*
//  * USAGE           : This will set up the socket addrress struct of the source destination
//  */
struct sockaddr_in set_up_dest_sockaddr( char *hostname, int port )
{
    //int clientfd;
    struct hostent *hp;
    struct sockaddr_in dest_sockaddr;

    hp = gethostbyname(hostname);

    bzero((char *) &dest_sockaddr, sizeof(dest_sockaddr));
    dest_sockaddr.sin_family = AF_INET;
    bcopy((char *)hp->h_addr,
          (char *)&dest_sockaddr.sin_addr.s_addr, hp->h_length);
    dest_sockaddr.sin_port = htons(port);


    return dest_sockaddr;
}



//     return &dest_sockaddr;
// }

/* Routine Name    : check_arguements
 * INPUT ARGUMENTS : 1. router_id 2.ne_host_name 3.ne_udp_port 4.router_udp_port  
 * RETURN VALUE    : 0 if succesful, -1 if failes
 * USAGE           : Check arguements to make sure they are correct
 */
int check_arguements(int router_id, char *ne_host_name, int ne_udp_port, int router_udp_port)
{
    if(router_id < 0 || router_id > MAX_ROUTERS-1)
    {
        perror("Router ID is invalid\n");
        return -1;
    }

    if(ne_host_name == NULL)
    {
        perror("Hostname is null\n");
        return -1;
    }

    if(ne_udp_port < 0 || ne_udp_port > 65535)
    {
        perror("ne UDP port is invalid\n");
        return -1;
    }

    if(router_udp_port < 0 || router_udp_port > 65535)
    {
        perror("router UDP port is invalid\n");
        return -1;
    }

    return 0;

}


int main(int argc, char** argv)
{

    int ne_udp_port, router_udp_port;
    char *ne_host_name;

    struct pkt_INIT_REQUEST init_request_pkt;
    struct pkt_INIT_RESPONSE init_response_pkt;
    

    pthread_t udp_thread;
    pthread_t timer_thread;

    int i;
    int ret;

    if(argc != 5)
    {
        perror( "Expected usage:\n ./router <router id> <ne hostname> <ne UDP port> <router UDP port>\n");
        return EXIT_FAILURE;
    }

    // initialize variables
    router_id = atoi(argv[1]);
    ne_host_name = argv[2];
    ne_udp_port = atoi(argv[3]);
    router_udp_port = atoi(argv[4]);

    // check if arguements are valid
    if(check_arguements(router_id, ne_host_name, ne_udp_port, router_udp_port) < 0)
    {
        perror("Error in arguments\n");
        return EXIT_FAILURE;
    }


    printf("router_id: %d, ne_host_name: %s, ne_udp_port: %d, router_udp_port: %d\n", router_id, ne_host_name, ne_udp_port, router_udp_port);
  

    //Step 1: set up ne struct address
    dest_sockaddr = set_up_dest_sockaddr(ne_host_name, ne_udp_port);

    //Step 2: bind to a udp port for listening and return fd 
    router_udp_fd = open_udp_fd(router_udp_port); 
    #ifdef DEBUG
    #if (DEBUG > 0)
        printf("listen UDP fd returned is %d\n ", router_udp_fd);
    #endif
    #endif
    
    if(router_udp_fd < 0 )
    {
        perror("Error when binding to a udp port\n");
        return EXIT_FAILURE;
    }

    //Step3: send INIT_REQUEST to ne (remember to convert it to network byte order)
    init_request_pkt.router_id = htonl(router_id);
    
    if( (ret = sendto(router_udp_fd, (char *) &init_request_pkt, sizeof(init_request_pkt), 0 ,
                               (struct sockaddr *) &dest_sockaddr, sizeof(dest_sockaddr)) ) < 0 )
    {
        perror("Error when sending in UDP\n");
        return EXIT_FAILURE;
    }

    dest_sockaddr_len = sizeof(dest_sockaddr);       
    //Step4: receive INIT_RESPONSE from ne (convert back to local byte order)
    bzero((void *)&init_response_pkt, sizeof(init_response_pkt));
    recvfrom(router_udp_fd, &init_response_pkt, sizeof(init_response_pkt), 0, 
           (struct sockaddr *) &dest_sockaddr, &dest_sockaddr_len);
    ntoh_pkt_INIT_RESPONSE(&init_response_pkt);
    

    printf("Number of neighbor I have %d \n",init_response_pkt.no_nbr);
    //Step 5: Initialize current router's routing table
    InitRoutingTbl(&init_response_pkt, router_id);
   
    
    //Step 6: Set up file pointer
    bzero((void*)final_logfile_name, sizeof(final_logfile_name));
    sprintf(final_logfile_name, "router%d.log", router_id);

    #ifdef DEBUG 
    #if (DEBUG > 0)
        printf("File name is %s\n",final_logfile_name);
    #endif    
    #endif

    // Initialize log file
    fptr = fopen(final_logfile_name,"w+");
    if(fptr == NULL)
    {
        perror("Error occured when opening file 1\n");
        while(1);
    }
    fclose(fptr);

    // Step 6: Begin threading
    num_of_neighbor = init_response_pkt.no_nbr;

    // now set up our user-define structure
    for(i = 0; i < num_of_neighbor; i++)
    {
        arr_my_neighbor[i].neighbor_id = init_response_pkt.nbrcost[i].nbr;
        arr_my_neighbor[i].my_cost_to_neighbor = init_response_pkt.nbrcost[i].cost;
        arr_my_neighbor[i].recent_time_updated = time(NULL);
        arr_my_neighbor[i].is_death = false;
    }


    // start the timer 
    beginning_time = time(NULL);
    last_update_interval = time(NULL);
    last_convergence_timeout_interval = time(NULL);

    // Initialize the lock
    pthread_mutex_init(&lock, NULL);
    
    // Initialize udp thread
    if (pthread_create(&udp_thread, NULL, udp_thread_function, NULL)) {
        perror("Error creating UDP thread");
        return EXIT_FAILURE;
    }

    // Initialize timer thread
    if (pthread_create(&timer_thread, NULL, timer_thread_function, NULL)) {
        perror("Error creating UDP thread");
        return EXIT_FAILURE;
    }

    pthread_join(udp_thread, NULL);
    pthread_join(timer_thread, NULL);
    pthread_mutex_destroy(&lock);

    close(router_udp_fd);

    return EXIT_SUCCESS;

}



/**
 * This will be udp threading function
 * 
 */
void *udp_thread_function(void * arg)
{
    bool found_sender_id;
    bool routing_table_has_changed;

    int i, ret;

    struct pkt_RT_UPDATE response_pkt;
    my_neighbor* neighbor_that_sent_update;

    while(1){

        // check if any neighbor is sending update packet to me
        errno = 0;
        bzero((void *)&response_pkt, sizeof(response_pkt));

        ret = recvfrom(router_udp_fd, &response_pkt, sizeof(response_pkt), 0, 
           (struct sockaddr *) &dest_sockaddr, &dest_sockaddr_len);

        // acquire lock
        if(ret < 0)
        {
            printf("ret %d\n", ret);
            perror("Error occured when receiving bytes from neighbors\n");
            fprintf(stderr, "Errno value is %d\n", errno);
            while(1);
        }

        ntoh_pkt_RT_UPDATE(&response_pkt);

        pthread_mutex_lock(&lock);

        #ifdef DEBUG 
        #if (DEBUG > 1)
        printf("/*********************************************/\n");
        printf("Number of bytes received %d\n",ret);
        printf("Sender id is: %d Dest id is: %d \n",response_pkt.sender_id, response_pkt.dest_id);
        for(i = 0; i < response_pkt.no_routes; i++ )
        {
            printf("From %hd to %hd, cost to neightbor: %hd, next_hop is %hd\n", 
            response_pkt.sender_id, response_pkt.route[i].dest_id,response_pkt.route[i].cost,response_pkt.route[i].next_hop);
        }
        printf("/*********************************************/\n");
        #endif    
        #endif

        // This is to prevent bugs
        if(response_pkt.sender_id != router_id)
        {
            // find sender_id index in current router's routing table
            i = 0;
            found_sender_id = false;
            // find what  the neighbor index is in arr_neighbor_id
            while (!found_sender_id  && i < num_of_neighbor) 
            {
                if (arr_my_neighbor[i].neighbor_id == response_pkt.sender_id)
                {
                    neighbor_that_sent_update = &arr_my_neighbor[i];
                    found_sender_id = true;
                }
                i++;
            }
            // set the neighbor live again
            neighbor_that_sent_update->is_death = false;
            
            // update the array of when the neighbor last send us its' routing table
            neighbor_that_sent_update->recent_time_updated = time(NULL);
            
            // update route 
            routing_table_has_changed =  UpdateRoutes(&response_pkt,neighbor_that_sent_update->my_cost_to_neighbor, response_pkt.dest_id);
            
            if(routing_table_has_changed)
            {
                #ifdef DEBUG
                #if (DEBUG > 0)
                    printf("Updated\n");
                #endif
                #endif
                last_convergence_timeout_interval = time(NULL);
                
                #ifdef DEBUG 
                #if (DEBUG > 0)
                    printf("File name is %s\n",final_logfile_name);
                #endif    
                #endif

                fptr = fopen(final_logfile_name,"a+");
                if(fptr == NULL)
                {
                    perror("Error occured when opening file 2\n");
                    while(1);
                }
                PrintRoutes(fptr,router_id);
                fclose(fptr);
                
                print_permission = true;
            }
            
        }
        
        pthread_mutex_unlock(&lock);
    }
}

/**
 * This function will send updates to neighbors
 */
void send_update_to_neighbor(int my_id)
{
    int i;
    int ret;
    struct pkt_RT_UPDATE response_pkt;

    for (i = 0; i < num_of_neighbor; i++)
    {
        // if it's death we don't need to send the update to it anymore
        if(!arr_my_neighbor[i].is_death)
        {
            #ifdef DEBUG
            #if (DEBUG > 0)
                printf("Sending to my routing table to neighbor %d\n",arr_my_neighbor[i].neighbor_id);
            #endif
            #endif

            bzero((void *)&response_pkt, sizeof(response_pkt));
            ConvertTabletoPkt(&response_pkt, my_id);
            response_pkt.dest_id = arr_my_neighbor[i].neighbor_id;
            hton_pkt_RT_UPDATE(&response_pkt);
            ret = sendto(router_udp_fd, (char *) &response_pkt, sizeof(response_pkt), 0 ,
                                    (struct sockaddr *) &dest_sockaddr, sizeof(dest_sockaddr));
            if(ret < 0)
            {
                perror("Error occured when sending update to neighbor");
            }
        }

    }
    

}

/**
 * This function will loop through the neighbor, see if neighbor last time 
 * send us update for more than FT seconds.
 * If so, mark neighbor as DEAD using UninstallRoutesOnNbrDeath
 */

// Bug: If the neighbor is already dead, then print permission will be false
void scan_for_dead_neighbors(time_t cur_time)
{
    int i;

    for (i = 0; i < num_of_neighbor; i++)
    {
        if ( ( cur_time - (arr_my_neighbor[i].recent_time_updated) ) >= FAILURE_DETECTION )
        {
            if(!arr_my_neighbor[i].is_death)
            {
                UninstallRoutesOnNbrDeath( arr_my_neighbor[i].neighbor_id );
                
                fptr = fopen(final_logfile_name,"a+");
                if(fptr == NULL)
                {
                    perror("Error occured when opening file 3\n");
                    while(1);
                }
                PrintRoutes(fptr,router_id);
                fclose(fptr);
                
                print_permission = true;
                last_convergence_timeout_interval = cur_time;

                #ifdef DEBUG
                #if (DEBUG > 0)
                    printf("Setting my neighbor %d to dead\n",arr_my_neighbor[i].neighbor_id);
                #endif
                #endif
                arr_my_neighbor[i].is_death = true;
            }
        }
    }
}

/**
 * This funciton will loop through the array to see if everything converged
 * Print out routing table is everything is converged
 * 
 * 
 */
// declare is_converged on top as global variable, set as false

// once converged, set the print permission to true after printing out, we set the print permission to false

// we set true:
// 1. when at least a neighbor dies
//     after uninstall route and print routes in scan_for_dead_neighbors
// 2. if our routing table changed (when UpdateRoutes returns 1)

void check_if_converged()
{

    // print out print_permission
    #ifdef DEBUG
    #if (DEBUG > 0)
        printf("print_permission: %d\n", print_permission);
    #endif
    #endif
    
    // if it returns 0 , then you would want to check if it has print permisisoin
    if (print_permission)
    {
        print_permission = false;

        fptr = fopen(final_logfile_name,"a+");
        if(fptr == NULL)
        {
            perror("Error occured when opening file 4\n");
            while(1);
        }
        fprintf(fptr,"%ld:Converged\n", time(NULL) - beginning_time);
        fflush(fptr);
        fclose(fptr);

        fprintf(stdout, "%ld:Converged\n", time(NULL) - beginning_time);

        
    }
}

/**
 * This will be timer threading function
 * 
 * Update: We want recvfrom to process fast, so having three locks will maximize that
 */
void * timer_thread_function(void * arg)
{

    while(1)
    {
        
        pthread_mutex_lock(&lock);  

        // Step 1: Scan for dead neighbors
        scan_for_dead_neighbors(time(NULL));
        
        // Step 2: check if (current time - last Update Interval) >= UI_const
        if ( (time(NULL) - last_update_interval) >= UPDATE_INTERVAL)
        {
            send_update_to_neighbor(router_id);
            last_update_interval = time(NULL);
        }

  
        // Step 3: check if (current time - last converged_interval) >= CT_const
        if ( (time(NULL)  - last_convergence_timeout_interval) >= CONVERGE_TIMEOUT )
        {
            #ifdef DEBUG
            #if (DEBUG > 0)
                printf("Checking if it has convereged\n");
            #endif
            #endif
            print_permission = 1;
            check_if_converged();
            last_convergence_timeout_interval = time(NULL);
        }

        pthread_mutex_unlock(&lock);
    }
}
