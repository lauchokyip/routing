#!/bin/bash

make clean
make
rm router*.log
gnome-terminal -e "./ne 8888 2_routers.conf"
gnome-terminal -e "./router 0 localhost 8888 8111"
gnome-terminal -e "./router 1 localhost 8888 8112"
# gnome-terminal -e "./router 2 localhost 8888 8113"
# gnome-terminal -e "./router 3 localhost 8888 8114"
# gnome-terminal -e "./router 4 localhost 8888 8115"
# gnome-terminal -e "./router 5 localhost 8888 8116"
# gnome-terminal -e "./router 6 localhost 8888 8117"
# gnome-terminal -e "./router 7 localhost 8888 8118"
# gnome-terminal -e "./router 8 localhost 8888 8119"
# gnome-terminal -e "./router 9 localhost 8888 8200"